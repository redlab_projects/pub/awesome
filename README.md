# REDLab Awesome list


> An awesome list is a list of awesome things curated by the community 


This list is a curated list of [![Awesome](https://awesome.re/badge.svg)](https://awesome.re) resources used in REDLab (Fablabs of Occitanie) network.
(inspired by [fabacademy awesome list](https://github.com/Academany/awesome-fabacademy))

Emoji Symbols used in this list
- :moneybag: `:moneybag:` = commercial
- :school: `:school:` = free for education
- :white_check_mark: `:white_check_mark:` = recommended
- :free: `:free:` = commercial but with free license (possibly limited)

**Table of Contents**

- [Awesome REDLab](#awesome-redlab)
  - [Communication](#communication)
  - [Collaborative tools](#collaborative-tools)

## Communication

### Visio

* [visio4you](http://www.visio4you.fr)

### Streaming

* [OBS - Open Broadcaster Software](https://obsproject.com/fr) (for Twitch, Youtube, ...)
## Collaborative tools

### Whiteboard

* [Openboard](https://www.openboard.ch/)

## IoT

### Data hosting

* [Adafruit](https://io.adafruit.com/)
* [Thingspeak](https://thingspeak.com/)

### Electronic simulations tool

* [Learn with TinkerCAD](https://www.tinkercad.com/learn/circuits)


